<?
function dump($var, $die = false, $all = false)
{
    global $USER;
    if ($USER->IsAdmin() || ($all = true))
    {
        ?>
        <pre>
            <?print_r($var);?>
        </pre>
        <?
    }
    if($die)
    {
        die;
    }
}
?>